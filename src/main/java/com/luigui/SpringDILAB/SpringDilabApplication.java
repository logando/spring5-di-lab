package com.luigui.SpringDILAB;

import com.luigui.SpringDILAB.controller.ConstrutorInjectedController;
import com.luigui.SpringDILAB.controller.MyController;
import com.luigui.SpringDILAB.controller.PropertyInjectedController;
import com.luigui.SpringDILAB.controller.SetterInjectedController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringDilabApplication {

 	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SpringDilabApplication.class, args);
		MyController myController =(MyController) ctx.getBean("myController");
		System.out.println("--------- Primary");
		System.out.println(myController.sayHello());

		System.out.println("--------- Property");
		PropertyInjectedController propertyInjectedController = (PropertyInjectedController) ctx.getBean("propertyInjectedController");
		System.out.println(propertyInjectedController.getGreeting());


		System.out.println("--------- Setter");
		SetterInjectedController setterInjectedController = (SetterInjectedController) ctx.getBean("setterInjectedController");
		System.out.println(setterInjectedController.getGreeting());


		System.out.println("--------- Constructor");
		ConstrutorInjectedController construtorInjectedController = (ConstrutorInjectedController) ctx.getBean("construtorInjectedController");
		System.out.println(construtorInjectedController.getGreeting());

	}

}
