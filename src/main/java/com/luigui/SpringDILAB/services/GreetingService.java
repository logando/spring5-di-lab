package com.luigui.SpringDILAB.services;

public interface GreetingService {
    String sayGreeting();
}
