package com.luigui.SpringDILAB.controller;

import com.luigui.SpringDILAB.services.GreetingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class ConstrutorInjectedController {
    private final GreetingService greetingService;

    public ConstrutorInjectedController(@Qualifier("constructorGreetingService") GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting(){
        return greetingService.sayGreeting();
    }
}
