package com.luigui.SpringDILAB.controller;

import com.luigui.SpringDILAB.services.ConstructorGreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConstrutorInjectedControllerTest {

    ConstrutorInjectedController controller;
    @BeforeEach
    void setUp() {
        controller = new ConstrutorInjectedController(new ConstructorGreetingService());
    }

    @Test
    void getGreeting() {
        System.out.println(controller.getGreeting());
    }
}